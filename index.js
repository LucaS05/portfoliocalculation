#! /usr/bin/env node
var portfolioCalc = require('./portfolioCalc.js');
var argv = process.argv.slice(2);
var portfolioID = argv[0];
var inputDate = argv[1];


if(portfolioID !== undefined && inputDate !== undefined){
	var portfolioData = require('fs').readFileSync('data/portfolios/' + portfolioID + '.json', 'utf8');
	var portfolioValue = portfolioCalc.calculateValue(portfolioData, inputDate);
	console.log(portfolioValue.toFixed(2));
} else {
	console.log('Per chiamare il comando:');
	console.log('node index.js <idportfolio> <data>');
}