module.exports.calculateValue = function(portfolioData, inputDate){
	var moment = require('moment');
	var securityValueFinder = require('./securityValueFinder.js');
	var portfolio = JSON.parse(portfolioData);
	var shares = [];
	var getValue = function(shares, date){
		var value = 0;
		if(shares !== undefined && date !== undefined){
			for(security in shares){
				var secVal = securityValueFinder.find(security, date);
				value += (shares[security] * secVal);
			}
		}
		return value;
	}
	var isSecurityPresent = function(securityID){
		return shares[securityID] !== undefined;
	}
	var addShares = function(securityID, sharesValue){
		if(isSecurityPresent(securityID)){
			shares[securityID] += sharesValue;
		}else{
			shares[securityID] = sharesValue;
		}
	}
	var removeShares = function(securityID, sharesValue){
		if(isSecurityPresent(securityID)){
			shares[securityID] -= sharesValue;
		}
	}
	portfolio.transactions.forEach(function(transaction){
		var transDate = moment(transaction.date);
		if(transDate.isSameOrBefore(inputDate)){
			var securityValue = securityValueFinder.find(transaction.securityId, transaction.date);
			if(securityValue > 0){
				if(!isSecurityPresent(transaction.securityId)){
					var sharesValue = transaction.amount / securityValue;
					addShares(transaction.securityId, sharesValue);
				} else {
					var sharesValue = transaction.amount / securityValue;
					if(transaction.type === "buy"){
						addShares(transaction.securityId, sharesValue);
					}else if(transaction.type === "sell"){
						removeShares(transaction.securityId, sharesValue);
					}
				}
			}
		}
	});
	return getValue(shares, inputDate);
}