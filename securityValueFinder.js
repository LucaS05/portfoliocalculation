module.exports.find = function(securityId, date){
	var moment = require('moment');
	var securityJson = JSON.parse(require('fs').readFileSync('data/securities/' + securityId + '.json', 'utf8'));
	var historyDetails = securityJson.historyDetails;
	var maxIndex = historyDetails.length - 1;
	var minIndex = 0;
	var value;

	var findValue = function(minIndex, maxIndex){
		var half = Math.floor((maxIndex + minIndex) / 2);
		if(moment(historyDetails[half].endDate).isSame(date)){
			return historyDetails[half].value;
		}
		if(minIndex < maxIndex){
			if(moment(date).isBefore(historyDetails[half].endDate)){
				value = findValue(minIndex, half - 1);
			} else {
				value = findValue(half + 1, maxIndex);
			}
		} else {
			value = -1;
		}
		return value;
	}

	return findValue(minIndex, maxIndex);
}